﻿import java.util.Scanner;

class Fahrkartenautomat {
	
	public static void main(String[] args) {
		Fahrkartenautomat fahrkartenautomat = new Fahrkartenautomat();
		Scanner tastatur = new Scanner(System.in);

		double zuZahlenderBetrag;
		double eingezahlterGesamtbetrag;
		//double eingeworfeneMuenze;
		//double rueckgabebetrag;
		int anzahlFahrkarten;
		double preisEinzelticket;
		
		System.out.print("Zu zahlender Betrag (EURO): ");
		String preisEinzelticketstring = tastatur.next();
		preisEinzelticket = Double.parseDouble(preisEinzelticketstring.replace(",", "."));
		if(preisEinzelticket < 1) {
			preisEinzelticket = 1;
			System.out.println("Preis kleiner als 1 ist unguelig");
		}
		System.out.print("Anzahl der Fahrkarten:");
		anzahlFahrkarten = tastatur.nextInt();
		if(anzahlFahrkarten < 1 || anzahlFahrkarten > 10) {
			anzahlFahrkarten = 1;
			System.out.println("Maximal 10 Fahrkarten Zulaessig");
		}	
		zuZahlenderBetrag = anzahlFahrkarten * preisEinzelticket;
		//zuZahlenderBetrag = tastatur.nextDouble();
		
		// Geldeinwurf
		eingezahlterGesamtbetrag = fahrkartenautomat.geldeinwurf(zuZahlenderBetrag, tastatur);

		// Fahrscheinausgabe
		fahrkartenautomat.fahrscheinausgabe();

		
		// Rückgeldberechnung und -Ausgabe
		fahrkartenautomat.rueckgeldberechnungAusgabe(zuZahlenderBetrag, eingezahlterGesamtbetrag);

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
				+ "Wir wuenschen Ihnen eine gute Fahrt.");
	}
	
	// Geldeinwurf
	private double geldeinwurf(double zuZahlenderBetrag, Scanner tastatur) {
		double eingeworfeneMuenze;
		double eingezahlterGesamtbetrag = 0.00;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			double BetragInsgesamt = zuZahlenderBetrag - eingezahlterGesamtbetrag;
			System.out.printf("Noch zu zahlen: %.2f Euro \n", BetragInsgesamt);
			System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
			eingeworfeneMuenze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneMuenze;
		}
		return eingezahlterGesamtbetrag;
	}
	
	// Fahrscheinausgabe
	private void fahrscheinausgabe() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 10; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}
	
	// Rückgeldberechnung und -Ausgabe
	private void rueckgeldberechnungAusgabe(double zuZahlenderBetrag, double eingezahlterGesamtbetrag) {
		double rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if(rueckgabebetrag > 0.0) {
			double RueckgabeInsgesamt = rueckgabebetrag;
			System.out.printf("Der Rückgabebetrag in Höhe von: %.2f Euro \n", RueckgabeInsgesamt);
			System.out.println("wird in folgenden Muenzen ausgezahlt:");

			double[] muenzgroesse = {2.0, 1.0, 0.5, 0.2, 0.1, 0.05};
			for(int i=0; i<muenzgroesse.length; i++) {
				double muenze = muenzgroesse[i];
				while (rueckgabebetrag >= muenze)
				{
					String eurocent = muenze>=1.0?"EURO":"CENT";
					System.out.println(muenze+" "+eurocent);
					rueckgabebetrag -= muenze;
				}
			}
			
//			while (rueckgabebetrag >= 2.0) // 2 EURO-Muenzen
//			{
//				System.out.println("2 EURO");
//				rueckgabebetrag -= 2.0;
//			}
//			while (rueckgabebetrag >= 1.0) // 1 EURO-Muenzen
//			{
//				System.out.println("1 EURO");
//				rueckgabebetrag -= 1.0;
//			}
//			while (rueckgabebetrag >= 0.5) // 50 CENT-Muenzen
//			{
//				System.out.println("50 CENT");
//				rueckgabebetrag -= 0.5;
//			}
//			while (rueckgabebetrag >= 0.2) // 20 CENT-Muenzen
//			{
//				System.out.println("20 CENT");
//				rueckgabebetrag -= 0.2;
//			}
//			while (rueckgabebetrag >= 0.1) // 10 CENT-Muenzen
//			{
//				System.out.println("10 CENT");
//				rueckgabebetrag -= 0.1;
//			}
//			while (rueckgabebetrag >= 0.05)// 5 CENT-Muenzen
//			{
//				System.out.println("5 CENT");
//				rueckgabebetrag -= 0.05;
//			}
		}
	}
}