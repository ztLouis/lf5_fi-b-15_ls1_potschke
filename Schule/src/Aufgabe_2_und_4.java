
/**
 * a) Würfel: V = a * a * a
 * b) Quader: V = a * b * c
 * c) Pyramide: V = a * a * h / 3
 * d) Kugel: V = 4/3 * r*r*r * PI. 
 * e) Zylinder: V = 2 * Math.PI * radius * hoehe
 * f) Rechteck: a * b * c
 * g) Dreieck: G=12a⋅b
 *
 */
public class Aufgabe_2_und_4 {

	private static String KOERPER_WUERFEL = "w"; 
	private static String KOERPER_KUGEL = "k"; 
	private static String KOERPER_PYRAMIDE = "p"; 
	private static String KOERPER_QUADER = "q"; 
	private static String KOEPER_ZYLINDER = "t";
	
	public static void main(String[] args) {
		Aufgabe_2_und_4 aufgabe = new Aufgabe_2_und_4();

		//multiplizieren test1
		double multi1 = 7.87;
		double multi2 = 2.36;
		double ergebnis = aufgabe.multiplizieren(multi1, multi2);
		System.out.println("Das Ergebnis der Rechnung ist: " + ergebnis);
		
		//multiplizieren test2
		multi1 = 5010.2;
		multi2 = 10.0;
		ergebnis = aufgabe.multiplizieren(multi1, multi2);
		System.out.println("Das Ergebnis der Rechnung ist: " + ergebnis);
		
		//dividieren test1
		double multi3 = 10;
		double multi4 = 0;
		Double ergebnis2 = aufgabe.dividieren(multi3,multi4); 
		System.out.println("Das Ergebnis der Rechnung ist: " + ergebnis2);
		
		double wuerfellaenge = 20.0;
		ergebnis = aufgabe.volumen(KOERPER_WUERFEL, wuerfellaenge, null, null);
		System.out.println("Das Ergebnis der Rechnung für Würfelvolumen ist: " + ergebnis);
		
		double kugelradius = 5.0;
		ergebnis = aufgabe.volumen(KOERPER_KUGEL, kugelradius, null, null);
		System.out.println("Das Ergebnis der Rechnung für Kugelvolumen ist: " + ergebnis);
		
		double pyramidenkantenlaenge = 5.0;
		double pyramidenhoehe = 10.0;
		ergebnis = aufgabe.volumen(KOERPER_PYRAMIDE, pyramidenkantenlaenge, pyramidenhoehe, null);
		System.out.println("Das Ergebnis der Rechnung für Pyramidenvolumen ist: " + ergebnis);
		
		double quaderlaenge = 2.0;
		double quaderbreite = 8.0;
		double quaderhoehe_ = 1.5;
		ergebnis = aufgabe.volumen(KOERPER_QUADER, quaderlaenge, quaderbreite, quaderhoehe_);
		System.out.println("Das Ergebnis der Rechnung für Quadervolumen ist: " + ergebnis);
		
		
		double zylinderradius = 2.0;
		double zylinderhoehe = 3.0;
		ergebnis = aufgabe.volumen(KOEPER_ZYLINDER, zylinderradius, zylinderhoehe, null);
		System.out.println("Das Ergebnis der Rechnung für Zylindervolumen ist: " + ergebnis);
		
		double rechteck_a = 3.0;
		double rechteck_b = 4.0;
		ergebnis = aufgabe.rechteck_seitec(rechteck_a, rechteck_b);
		System.out.println("Das Ergebnis der Rechnung für c ist: " + ergebnis);
		ergebnis = aufgabe.rechteck_flaeche(rechteck_a, rechteck_b);
		System.out.println("Das Ergebnis der Rechnung für Fläche ist: " + ergebnis);
	}
	
	public double multiplizieren(double faktor1, double faktor2) {
		return faktor1 * faktor2;
	}
	
	public Double dividieren(double faktor1, double faktor2) {
		Double resultat;
		if(faktor2==0.00) {
			resultat = null;
		}
		else {
			resultat = faktor1 / faktor2;
		}
		return resultat;
	}
	
	public double volumen(String type, Double wert1, Double wert2, Double wert3) {
		double ergebnis = 0.00;
		if(type.equals(KOERPER_WUERFEL)) {
			ergebnis = volumen_wuerfel(wert1.doubleValue());
		}
		else if(type.equals(KOERPER_KUGEL)) {
			ergebnis = volumen_kugel(wert1.doubleValue());
		}
		else if(type.equals(KOERPER_PYRAMIDE)) {
			ergebnis = volumen_pyramide(wert1.doubleValue(), wert2.doubleValue());
		}
		else if(type.equals(KOERPER_QUADER)) {
			ergebnis = volumen_quader(wert1.doubleValue(), wert2.doubleValue(), wert3.doubleValue());
		}
		else if(type.equals(KOEPER_ZYLINDER)) {
			ergebnis = volumen_zylinder(wert1.doubleValue(), wert2.doubleValue());
		}
		return ergebnis;
	}

	private double volumen_zylinder(double radius, double hoehe) {
		return 2 * Math.PI * radius * hoehe; 
	}

	private double volumen_wuerfel(double a) {
		return a * a * a;
	}

	private double volumen_quader(double a, double b, double c) {
		return a * b * c;
	}

	private double volumen_pyramide(double a, double h) {
		return a * a * h / 3.0;
	}

	private double volumen_kugel(double r) {
		return 4.0/3.0 * r*r*r * Math.PI;
	}
	
	private double rechteck_seitec(double a, double b) {
		return Math.hypot(a, b);
	}
	
	private double rechteck_flaeche(double a, double b)	{
		return a * b / 2; 
	}
}