/**
 * 
 * @author poetschke
 */
public class Temperatur {

	/**
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Temperatur temperatur = new Temperatur();
		double[][] data = temperatur.getTestdaten(); 
		temperatur.ausgabe(data);
	}

	/**
	 * 
	 * @param data
	 */
	private void ausgabe(double[][] data) {
		System.out.printf("%-12s|%10s \n", "Fahrenheit", "Celsius");
		
		System.out.println("------------------------"); //24 Zeichen -> 12 fuer Fahrenheit, 1 fuer Trenner, 10 fuer Celsius, 1 fuer letztes Zeichen
		
		for(int i=0; i<data.length; i++) {
			System.out.printf("%+-12.0f|%+10.2f \n", data[i][0], data[i][1]);
		}
	}
	
	/**
	 * 
	 * @return
	 */
	private double[][] getTestdaten() {
		double[][] data = { { -20.0, -28.8889 },
							{ -10.0, -23.3333 },
							{ 0.0, -17.7778 },
							{ 20.0, -6.6667 },
							{ 30.0, -1.1111 }};
		return data;
	}
}