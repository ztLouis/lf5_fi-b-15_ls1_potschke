import java.util.Scanner;

	public class Noten_und_Monate {

		public static void main(String[] args) {
		Noten_und_Monate notenmonate = new Noten_und_Monate();
		Scanner myScanner = new Scanner(System.in);
		String input = myScanner.next();



		notenmonate.zeigeNote(input);

		myScanner.close();
		}

		private void zeigeNote(String note) {
		String[][] noten = {{"1","Sehr gut"}, {"2","Gut"}, {"3","Befriedigend"}, {"4","Ausreichend"}, {"5","Mangelhaft"}, {"6","Ungenügend"}};
		String noten_text = null;
		for(int i=0; i<6; i++) {
		String noten_note = noten[i][0];
		if(noten_note.equals(note)) {
		noten_text = noten[i][1];
		}
		}
		if(noten_text==null) {
		noten_text = "Fehler. Note nicht gefunden";
		}
		System.out.println(noten_text);
		}
		}
