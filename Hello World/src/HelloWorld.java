
public class HelloWorld {

	public static void main(String[] args) {

		System.out.println("Hello World");
		System.out.println("Guten Tag Louis");
		System.out.println("Willkommen in der Veranstaltung Strukturierte Programmierung.");
		System.out.println("Aufgabe 1");
		System.out.println("");
		System.out.println("Er sagte: \"Guten Tag!\""); 
		//System.out.println("Dies ist ein Kommentar");
		System.out.println(""); 
		String s = "*";
		String c = "***";
		String x = "*****";
		String cc = "*******";
		String sss = "*********";
		String xx = "***********";
		String ccc = "*************";
		System.out.println("Aufgabe 2");
		System.out.println(""); 
		System.out.printf( "%10s\n", s );
		System.out.printf( "%11s\n", c );
		System.out.printf( "%12s\n", x );
		System.out.printf( "%13s\n", cc );
		System.out.printf( "%14s\n", sss );
		System.out.printf( "%15s\n", xx );
		System.out.printf( "%16s\n", ccc );
		System.out.printf( "%11s\n", c );
		System.out.printf( "%11s\n", c );
	}

}
