import java.util.Scanner;

public class Quadrieren {

	public static void main(String[] args) {

		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ===========================
		titel();
		double x = eingabe();
		
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		double ergebnis = verarbeitung(x);
		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		ausgabe(x, ergebnis);
	}
	public static double verarbeitung(double x) {
		double ergebnis = x * x;
		return ergebnis;
	}

	public static double eingabe() {
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Bitte eine Zahl eingebn; ");
		double x = tastatur.nextDouble();
		return x; 
	}
 public static void titel() {
	System.out.println("Dieses Programm berechnet die Quadratzahl x�");
	System.out.println("---------------------------------------------");
 }

	public static void ausgabe(double x, double ergebnis) {
		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);
	}
}
